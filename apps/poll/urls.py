from django.urls import path

from rest_framework.routers import SimpleRouter

from apps.poll.views import (
    PollReadOnlyModelViewSet, UserPollRelationUpdateAPIView,
    UserPollRelationListAPIView
)

app_name = 'poll'

router = SimpleRouter()
router.register(r'', PollReadOnlyModelViewSet, basename='poll')

urlpatterns = [
    path(
        'user_answer/', UserPollRelationListAPIView.as_view(),
        name='user_answer_list'
    ),
    path(
        'user_answer/<int:pk>/', UserPollRelationUpdateAPIView.as_view(),
        name='user_answer'
    )
]

urlpatterns += router.urls
