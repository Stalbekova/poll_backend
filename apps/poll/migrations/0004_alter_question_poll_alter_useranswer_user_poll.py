# Generated by Django 4.0.4 on 2022-04-17 08:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('poll', '0003_alter_choice_options_remove_choice_position'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='poll',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='poll.poll'),
        ),
        migrations.AlterField(
            model_name='useranswer',
            name='user_poll',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_answers', to='poll.userpollrelation'),
        ),
    ]
