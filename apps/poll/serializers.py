from rest_framework import serializers

from poll_backend.constants import (
    TEXT_ANSWER, SINGLE_CHOICE, MULTIPLE_CHOICE
)
from apps.poll.models import (
    Poll, Question, Choice, UserAnswer, UserPollRelation
)


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'choice_text')


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(
        Question.objects.all().order_by('position'), many=True
    )

    class Meta:
        model = Question
        fields = ('id', 'question_text', 'type', 'choices')


class PollListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ('id', 'name', 'description', 'start_date', 'expiration_date')


class PollRetrieveSerializer(PollListSerializer):
    questions = QuestionSerializer(many=True)

    class Meta(PollListSerializer.Meta):
        fields = PollListSerializer.Meta.fields + ('questions', )


class UserAnswerSerializer(serializers.ModelSerializer):
    question = serializers.PrimaryKeyRelatedField(
        queryset=Question.objects.all()
    )
    choice_answer = serializers.PrimaryKeyRelatedField(
        queryset=Choice.objects.all(), many=True, required=False
    )

    class Meta:
        model = UserAnswer
        fields = ('question', 'text_answer', 'choice_answer')

    def validate(self, data):
        question = data.get('question')

        if question.type == TEXT_ANSWER and not data.get('text_answer'):
            raise serializers.ValidationError({
                'question': question.pk,
                'text_answer': 'Ответ на этот вопрос в текстовом формате'
            })
        elif question.type == SINGLE_CHOICE:
            if not data.get('choice_answer') or len(
                    data.get('choice_answer')) != 1:
                raise serializers.ValidationError({
                    'question': question.pk,
                    'choice_answer': 'Выберите один вариант'
                })
        elif question.type == MULTIPLE_CHOICE and not data.get(
                'choice_answer'):
            raise serializers.ValidationError({
                'question': question.pk,
                'choice_answer': 'Выберите хотя бы один один вариант'
            })
        elif question.type in [SINGLE_CHOICE, MULTIPLE_CHOICE] and data.get(
                'choice_answer'):
            for choice in data.get('choice_answer'):
                if choice not in question.choices.all():
                    raise serializers.ValidationError({
                        'question': question.pk,
                        'choice_answer': (
                            f'Недопустимый первичный ключ {choice.pk}'
                        )
                    })
        return data


class UserPollRelationUpdateSerializer(serializers.ModelSerializer):
    user_answers = UserAnswerSerializer(many=True, required=True)

    class Meta:
        model = UserPollRelation
        fields = ('user_answers', )

    def update(self, instance, validated_data):
        user_answers = validated_data.pop('user_answers')

        instance_qs_ids = list(
            instance.poll.questions.all().values_list('id', flat=True)
        )
        val_data_qs_ids = [
            user_answer.get('question').pk
            for user_answer in user_answers
        ]
        val_data_qs_ids.sort()

        if instance_qs_ids != val_data_qs_ids:
            if len(val_data_qs_ids) < len(instance_qs_ids):
                raise serializers.ValidationError({
                    'detail': f'Вы не ответили на все вопросы'
                })
            else:
                raise serializers.ValidationError({
                    'detail': f'Недопустимый первичный ключ'
                })

        for user_answer in user_answers:
            user_answer_obj, _ = UserAnswer.objects.update_or_create(
                user_poll=instance,
                question=user_answer.get('question'),
                defaults={'text_answer': user_answer.get('text_answer')}
            )
            if user_answer.get('choice_answer'):
                user_answer_obj.choice_answer.clear()
                for choice in user_answer.get('choice_answer'):
                    user_answer_obj.choice_answer.add(choice)

        return instance


class UserAnswerDetailSerializer(serializers.ModelSerializer):
    question = QuestionSerializer()

    class Meta:
        model = UserAnswer
        fields = ('question', 'text_answer', 'choice_answer')


class UserPollRelatioListSerializer(serializers.ModelSerializer):
    poll = PollListSerializer()
    user_answers = UserAnswerDetailSerializer(many=True)

    class Meta:
        model = UserPollRelation
        fields = ('poll', 'user_answers')
