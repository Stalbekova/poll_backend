from django.utils.decorators import method_decorator

from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated

from apps.poll.models import Poll, UserPollRelation
from apps.poll.serializers import (
    PollListSerializer, PollRetrieveSerializer,
    UserPollRelationUpdateSerializer, UserPollRelatioListSerializer
)
from apps.poll.paginations import ListPagination


@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_description='Эндпоинт для получения списка активных опросов'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    operation_description=(
            'Эндпоинт для получения списка активных опросов с детализацией'
    )
))
class PollReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    pagination_class = ListPagination
    queryset = (
        Poll.active_objects.prefetch_related('questions').all()
    )

    def get_serializer_class(self):
        if self.action == 'list':
            serializer_class = PollListSerializer
        else:
            serializer_class = PollRetrieveSerializer
        return serializer_class


class UserPollRelationUpdateAPIView(generics.UpdateAPIView):
    """
    Эндпоинт для прохождения опроса
    /poll/user_answer/{id}/  здесь {id} ---> id опроса
    """

    queryset = Poll.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = UserPollRelationUpdateSerializer

    def get_object(self):
        poll = super(UserPollRelationUpdateAPIView, self).get_object()
        user_poll, created = UserPollRelation.objects.get_or_create(
            user=self.request.user, poll=poll
        )
        return user_poll


class UserPollRelationListAPIView(generics.ListAPIView):
    """
    Эндпоинт для Получение пройденных пользователем
    опросов с детализацией по ответам
    """

    permission_classes = (IsAuthenticated,)
    serializer_class = UserPollRelatioListSerializer

    def get_queryset(self):
        qs = (
            UserPollRelation.objects.prefetch_related(
                'user_answers', 'poll', 'user_answers__question',
                'user_answers__question__choices'
            ).filter(user=self.request.user)
        )
        return qs
