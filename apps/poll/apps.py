from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'apps.poll'
    verbose_name = 'Опрос'

