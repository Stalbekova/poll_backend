import random
import factory

from apps.account.tests.factories import UserFactory
from apps.poll.models import (
    Poll, Question, Choice, UserAnswer, UserPollRelation
)


class PollFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('name')
    description = factory.Faker('text')
    expiration_date = factory.Faker('date_time')

    class Meta:
        model = Poll


class QuestionFactory(factory.django.DjangoModelFactory):
    poll = factory.SubFactory(PollFactory)
    question_text = factory.Faker('name')
    position = random.randrange(1, 1000)

    class Meta:
        model = Question


class ChoiceFactory(factory.django.DjangoModelFactory):
    question = factory.SubFactory(QuestionFactory)
    choice_text = factory.Faker('name')

    class Meta:
        model = Choice


class UserPollRelationFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    poll = factory.SubFactory(PollFactory)

    class Meta:
        model = UserPollRelation


class UserAnswerFactory(factory.django.DjangoModelFactory):
    user_poll = factory.SubFactory(UserPollRelationFactory)
    question = factory.SubFactory(QuestionFactory)
    text_answer = factory.Faker('name')
    choice_answer = factory.SubFactory(ChoiceFactory)

    class Meta:
        model = UserAnswer
