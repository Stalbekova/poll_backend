from django.urls import reverse
from django.utils import timezone

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import (
    APITestCase, APIRequestFactory, force_authenticate,
)

from apps.account.tests.factories import UserFactory
from apps.poll.models import Poll
from apps.poll.tests.factories import (
    PollFactory, QuestionFactory, ChoiceFactory, UserPollRelationFactory
)
from apps.poll.serializers import (
    PollListSerializer, PollRetrieveSerializer, UserPollRelatioListSerializer
)
from apps.poll.views import PollReadOnlyModelViewSet
from apps.poll.serializers import UserPollRelationUpdateSerializer
from poll_backend.constants import (
    TEXT_ANSWER, SINGLE_CHOICE, MULTIPLE_CHOICE
)


factory = APIRequestFactory()


class TestPollReadOnlyModelViewSet(APITestCase):

    def setUp(self) -> None:
        self.user = UserFactory()
        self.poll_1 = PollFactory(
            expiration_date=timezone.now()+timezone.timedelta(days=20)
        )
        self.poll_2 = PollFactory(
            expiration_date=timezone.now()+timezone.timedelta(days=30))
        self.poll_3 = PollFactory(
            expiration_date=timezone.now()-timezone.timedelta(days=10)
        )
        self.token, _ = Token.objects.get_or_create(user=self.user)

        self.url_1 = reverse('v1:poll:poll-list')
        self.url_2 = reverse('v1:poll:poll-detail', kwargs={'pk': self.poll_1.pk})

    def test_get_active_poll_list(self) -> None:
        view = PollReadOnlyModelViewSet.as_view({'get': 'list'})
        request = factory.get(self.url_1)
        response = view(request)
        response.render()

        expected_data = {
            'count': 2,
            'next': None,
            'previous': None,
            'results': PollListSerializer(
                [self.poll_1, self.poll_2], many=True
            ).data
        }

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)

    def test_get_poll_retrieve(self) -> None:
        view = PollReadOnlyModelViewSet.as_view({'get': 'retrieve'})
        request = factory.get(self.url_2)
        force_authenticate(request, user=self.user, token=self.token)
        response = view(request, pk=self.poll_1.pk)
        response.render()
        expected_data = PollRetrieveSerializer(
            self.poll_1
        ).data

        self.assertEqual(expected_data, response.data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_poll_if_model_empty(self):
        Poll.objects.all().delete()
        response = self.client.get(self.url_1)
        expected_data = {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        }

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)


class TestUserPollRelationAPIView(APITestCase):
    def setUp(self) -> None:

        self.user = UserFactory()

        self.poll = PollFactory(
            name='test poll',
            description='poll description',
            expiration_date=timezone.now()+timezone.timedelta(days=20)
        )
        self.question_1 = QuestionFactory(
            poll=self.poll,
            question_text='What is your name?',
            type=TEXT_ANSWER
        )
        self.question_2 = QuestionFactory(
            poll=self.poll,
            question_text='How old are you?',
            type=SINGLE_CHOICE
        )
        self.choice_1 = ChoiceFactory(
            question=self.question_2,
            choice_text='18-25'
        )
        self.choice_2 = ChoiceFactory(
            question=self.question_2,
            choice_text='25-35'
        )
        self.choice_3 = ChoiceFactory(
            question=self.question_2,
            choice_text='35 >'
        )
        self.question_3 = QuestionFactory(
            poll=self.poll,
            question_text='What are your favorite fruits?',
            type=MULTIPLE_CHOICE
        )
        self.choice_4 = ChoiceFactory(
            question=self.question_3,
            choice_text='apple'
        )
        self.choice_5 = ChoiceFactory(
            question=self.question_3,
            choice_text='banana'
        )
        self.choice_6 = ChoiceFactory(
            question=self.question_3,
            choice_text='peach'
        )
        self.user_poll = UserPollRelationFactory(
            user=self.user, poll=self.poll
        )
        self.url_1 = reverse('v1:poll:user_answer_list')
        self.url_2 = reverse('v1:poll:user_answer', kwargs={'pk': self.poll.pk})
        self.token = Token.objects.create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_user_answer_put_valid_on_200(self):
        data = {
            "user_answers": [
                {
                    "question": self.question_1.pk,
                    "text_answer": "text_answer"
                },
                {
                    "question": self.question_2.pk,
                    "choice_answer": [1]
                },
                {
                    "question": self.question_3.pk,
                    "choice_answer": [6]
                }
            ]
        }

        response = self.client.put(self.url_2, data=data, format='json')
        expected_data = UserPollRelationUpdateSerializer(self.user_poll).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)

    def test_user_answer_get_valid_on_200(self):
        data = {
            "user_answers": [
                {
                    "question": self.question_1.pk,
                    "text_answer": "text_answer"
                },
                {
                    "question": self.question_2.pk,
                    "choice_answer": [self.question_1.pk]
                },
                {
                    "question": self.question_3.pk,
                    "choice_answer": [self.choice_4.pk, self.choice_6.pk]
                }
            ]
        }

        self.client.put(self.url_2, data=data, format='json')
        response = self.client.get(self.url_1)
        expected_data = UserPollRelatioListSerializer(
            self.user.user_polls.all(), many=True
        ).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)
