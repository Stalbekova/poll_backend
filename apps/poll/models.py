from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from poll_backend.constants import QUESTION_TYPE


class PollManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            expiration_date__gte=timezone.now()
        )


class Poll(models.Model):
    """
    Poll Model
    """
    name = models.CharField(max_length=255, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    start_date = models.DateTimeField(
        verbose_name='Дата старта', default=timezone.now
    )
    expiration_date = models.DateTimeField(verbose_name='Дата окончания')

    objects = models.Manager()
    active_objects = PollManager()

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'

    def __str__(self):
        return self.name

    def clean(self):
        if self.start_date >= self.expiration_date:
            raise ValidationError(
                'Неверное значение в дате начала и дате окончания'
            )


class Question(models.Model):
    """
    Question Model
    """
    poll = models.ForeignKey(
        'poll.Poll', on_delete=models.CASCADE, related_name='questions'
    )
    question_text = models.CharField(
        max_length=255, verbose_name='Текст вопроса'
    )
    type = models.CharField(
        max_length=20, verbose_name='Тип', choices=QUESTION_TYPE
    )
    position = models.PositiveIntegerField(
        default=0, blank=True, null=False, verbose_name='№'
    )

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'
        ordering = ('position',)

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    """
    Model for answer options for the question
    """
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name='choices'
    )
    choice_text = models.CharField(max_length=255, verbose_name='Текст')

    class Meta:
        verbose_name = 'Выбор'
        verbose_name_plural = 'Выбор'

    def __str__(self):
        return self.choice_text


class UserPollRelation(models.Model):
    """
    Model user poll relation
    """
    user = models.ForeignKey(
        'account.User', on_delete=models.CASCADE, verbose_name='Пользователь',
        related_name='user_polls'
    )
    poll = models.ForeignKey(
        Poll, on_delete=models.CASCADE, verbose_name='Опрос'
    )

    class Meta:
        verbose_name = 'Опрос пользователя'
        verbose_name_plural = 'Опросы пользователей'

    def __str__(self):
        return self.user.email


class UserAnswer(models.Model):
    """
    Model user answer
    """
    user_poll = models.ForeignKey(
        UserPollRelation, on_delete=models.CASCADE, related_name='user_answers'
    )
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, verbose_name='Вопрос'
    )
    text_answer = models.CharField(max_length=255, blank=True, null=True)
    choice_answer = models.ManyToManyField(Choice, blank=True, null=True)

    class Meta:
        verbose_name = 'Ответ пользователя'
        verbose_name_plural = 'Ответы пользователей'

    def __str__(self):
        return self.question.question_text
