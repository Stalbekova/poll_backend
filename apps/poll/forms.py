from django import forms
from django.forms.models import BaseInlineFormSet

from poll_backend.constants import SINGLE_CHOICE, MULTIPLE_CHOICE
from apps.poll.models import UserAnswer, Choice


class ChoiceInlineFormSet(BaseInlineFormSet):
    """
    This formset class is for check count forms
    """

    def __init__(self, *args, **kwargs):
        super(ChoiceInlineFormSet, self).__init__(*args, **kwargs)

    def clean(self):
        if self.instance.type in [SINGLE_CHOICE, MULTIPLE_CHOICE]:
            count = 0
            for form in self.forms:
                try:
                    if form.cleaned_data:
                        count += 1
                except AttributeError:
                    pass
            if count < 2:
                raise forms.ValidationError(
                    'Введите варианты ответа на вопрос (min 2)'
                )


class UserAnswerForm(forms.ModelForm):
    class Meta:
        model = UserAnswer
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UserAnswerForm, self).__init__(*args, **kwargs)
        try:
            self.fields['choice_answer'].queryset = Choice.objects.filter(
                question_id=self.instance.question.pk
            )
        except Exception:
            pass
