from django.contrib import admin

from nested_inline.admin import (
    NestedStackedInline, NestedModelAdmin, NestedTabularInline
)

from apps.poll.forms import ChoiceInlineFormSet, UserAnswerForm
from apps.poll.models import (
    Poll, Question, Choice, UserPollRelation, UserAnswer
)


class ChoiceInline(NestedTabularInline):
    model = Choice
    fields = ('choice_text',)
    formset = ChoiceInlineFormSet
    extra = 0
    verbose_name_plural = '... Выбор'


class QuestionInline(NestedStackedInline):
    model = Question
    fields = ('position', 'question_text', 'type')
    extra = 1
    inlines = [ChoiceInline]


@admin.register(Poll)
class PollAdmin(NestedModelAdmin):
    list_display = (
        'id', 'name', 'start_date', 'expiration_date'
    )
    list_display_links = list_display
    inlines = [QuestionInline]


class UserAnswerInline(admin.StackedInline):
    model = UserAnswer
    form = UserAnswerForm
    extra = 0


@admin.register(UserPollRelation)
class UserPollRelationAdmin(admin.ModelAdmin):
    inlines = [UserAnswerInline]
    list_display = ('id', 'user', 'poll')
    list_display_links = list_display

    def get_queryset(self, request):
        return (
            super(UserPollRelationAdmin, self).get_queryset(request)
                .prefetch_related(
                'user_answers', 'poll', 'user_answers__question',
                'user_answers__question__choices'
            )
        )
