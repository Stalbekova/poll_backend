from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone

from apps.account.managers import CustomUserManager
from poll_backend.constants import GENDER_TYPE


class User(AbstractBaseUser, PermissionsMixin):
    """
    User who login via email
    """
    email = models.EmailField(verbose_name='Почта', unique=True)
    first_name = models.CharField(verbose_name='Имя', max_length=255)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255)
    gender = models.CharField(
        max_length=25, verbose_name='Пол', choices=GENDER_TYPE,
    )
    is_active = models.BooleanField(verbose_name='Активный', default=True)
    is_staff = models.BooleanField(
        verbose_name='Статус персонала', default=False
    )
    date_joined = models.DateTimeField(
        verbose_name='Зарегистрирован', default=timezone.now
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.email

    @property
    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    @property
    def get_short_name(self):
        """
        Return the short name for the user.
        """
        return self.first_name
