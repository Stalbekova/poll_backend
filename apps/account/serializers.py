from django.contrib.auth.password_validation import validate_password
from django.utils.translation import gettext_lazy as _

from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

from apps.account.models import User


class UserAuthSerializer(serializers.Serializer):
    email = serializers.EmailField(write_only=True)
    password = serializers.CharField(write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        try:
            user = User.objects.get(email=data.get('email'))

            if not user.is_active:
                raise serializers.ValidationError(
                    {'email': 'Этот номер не активен.'}
                )
            if not user.check_password(data.get('password')):
                raise serializers.ValidationError(
                    {'password': 'Неверный пароль'}
                )

        except User.DoesNotExist:
            raise serializers.ValidationError(
                {'email': 'Пользователь не найден'}
            )

        token, created = Token.objects.get_or_create(user=user)

        return {'token': token.key}


class UserAuthAPIViewResponseSerializer(serializers.Serializer):
    token = serializers.CharField()


class UserRegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'id', 'email', 'password', 'password2', 'first_name',
            'last_name', 'gender'
        )

    def validate(self, data):
        if data.get('password') != data.get('password2'):
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})

        return data

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            gender=validated_data['gender']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class AuthErrorSerializer(serializers.Serializer):
    """ Error response on status.HTTP_401_UNAUTHORIZED """
    detail = serializers.CharField(help_text='This is error text')


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'gender')
