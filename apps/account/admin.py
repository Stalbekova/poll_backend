from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from apps.account.models import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': (
            'email', 'password'
        )}),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name', 'gender')
        }),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser')
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')})
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email', 'first_name', 'last_name', 'password1',
                'password2'
            )
        }),
    )
    list_display = (
       'id', 'email', 'first_name', 'last_name', 'date_joined'
    )
    list_display_links = list_display
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email', )

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return ()
        return ('email', )
