from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from apps.account.models import User
from apps.account.serializers import (
    UserAuthSerializer, UserAuthAPIViewResponseSerializer,
    UserRegisterSerializer, UserProfileSerializer, AuthErrorSerializer
)


class UserAuthAPIView(generics.GenericAPIView):
    """ Эндпоинт для login """
    serializer_class = UserAuthSerializer

    @swagger_auto_schema(
        responses={
            200: UserAuthAPIViewResponseSerializer(),
            400: 'It will return error type'
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class UserRegisterAPIView(generics.CreateAPIView):
    """ Эндпоинт для регистрации пользователя"""
    serializer_class = UserRegisterSerializer


class UserProfileAPIView(generics.RetrieveAPIView):
    """ Эндпоинт для профиля пользователя"""
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserProfileSerializer

    def get_object(self):
        return self.request.user

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                'Authorization', openapi.IN_HEADER,
                description='Token ...',
                type=openapi.TYPE_STRING
            )
        ],
        responses={401: AuthErrorSerializer()}
    )
    def get(self, request, *args, **kwargs):
        return super(UserProfileAPIView, self).get(request, *args, **kwargs)
