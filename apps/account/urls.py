from django.urls import path

from apps.account.views import (
    UserAuthAPIView, UserRegisterAPIView, UserProfileAPIView
)

app_name = 'account'

urlpatterns = [
    path('login/', UserAuthAPIView.as_view(), name='login'),
    path('registration/', UserRegisterAPIView.as_view(), name='registration'),
    path('profile/', UserProfileAPIView.as_view(), name='profile'),
]
