from django.urls import reverse

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase

from apps.account.tests.factories import UserFactory


class TestUserAuthAPIView(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('v1:account:login')
        cls.user = UserFactory(
            email='example@gmail.com',
            password='example12345'
        )

    def test_user_does_not_exists_on_200(self):
        data = {'email': 'test@gmail.com', 'password': 'example12345'}
        response = self.client.post(self.url, data=data)
        expected_data = {
            'email': [
                ErrorDetail(string='Пользователь не найден', code='invalid')
            ]
        }

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(expected_data, response.data)

    def test_check_invalid_password_400(self):
        data = {
            'email': 'example@gmail.com',
            'password': 'example1234567890'
        }
        response = self.client.post(self.url, data=data)
        expected_data = {
            'password': [ErrorDetail(string='Неверный пароль', code='invalid')]
        }

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(expected_data, response.data)

    def test_user_exists_on_200(self):
        user = UserFactory(email='example1@gmail.com')
        user.set_password('example12345')
        user.save()

        data = {
            'email': 'example1@gmail.com',
            'password': 'example12345'
        }
        response = self.client.post(self.url, data=data)
        token, created = Token.objects.get_or_create(user=user)
        expected_data = {
            'token': token.key
        }

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)
