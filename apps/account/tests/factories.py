import factory

from apps.account.models import User
from poll_backend.constants import MALE


class UserFactory(factory.django.DjangoModelFactory):
    email = factory.Faker('email')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    gender = MALE

    class Meta:
        model = User
