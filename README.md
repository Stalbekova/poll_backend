# Poll Backend

# Описание проекта
Административная панель а также `API` 
для прохождение опросов пользователей


# Как поднять локально проект?
Зависимости:
- PostgreSQL
- Python 3.8
- Django 4.0


## Переменные окружения для продакшена, для локальной разработки можно и не указывать:
| Key    | Description   |    Default value  |
| :---         |     :---      |          :--- |
| `DJANGO_SECRET_KEY`  | secret key  | secret-key              |
| `DJANGO_DEBUG`  | Debug mode True or False  | True              |
| `DJANGO_ALLOWED_HOST`| Allowed host | 0.0.0.0,127.0.0.1 |
| `DEFAULT_DATABASE_URL`  | postgres://user:password@host:port/database_name | postgres://postgres:postgres@db:5432/poll_db |

## Последовательность действий
```.bash
    $ git clone https://gitlab.com/Stalbekova/poll_backend.git
    $ cd poll_backend/ 
    $ virtualenv venv
    $ pip install -r requirements.txt
```
Необходимо создать в PostgreSQL создать БД:
```.bash
    $ sudo -u postgres psql
    $ create database poll_db encoding 'UTF-8';
    $ \q
```
После создания БД, необходимо применить миграцию, после запуск тестового сервера:
```.bash
    $ python manage.py migrate
    $ python manage.py createsuperuser
    $ python manage.py runserver
```
Если все успешно то переходите по ссылке ==> `http://locahost:8000`

Документация по API ==> `http://locahost:8000/docs/`
