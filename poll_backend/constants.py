MALE = 'male'
FEMALE = 'female'

GENDER_TYPE = (
    (MALE, 'Мужчина'),
    (FEMALE, 'Женщина')
)

TEXT_ANSWER = 'text_answer'
SINGLE_CHOICE = 'single_choice'
MULTIPLE_CHOICE = 'multiple_choice'

QUESTION_TYPE = (
    (TEXT_ANSWER, 'ответ текстом'),
    (SINGLE_CHOICE, 'ответ с выбором одного варианта'),
    (MULTIPLE_CHOICE, 'ответ с выбором нескольких вариантов')
)
